# FFLM Signature

Juste un script pour la réalisation d'une signature chez FFLM

## Utilisation
Copier / coller le contenu du fichier signature-fflm.sh et l'executer
```
bash signature-fflm.sh
```

Ou passer par curl
```
curl https://framagit.org/CyrilleBiot/fflm-signature/-/raw/main/signature-fflm.sh?ref_type=heads | bash
```
