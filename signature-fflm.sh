#!/bin/bash 

# Test de l'existance du paquet xclip (copier / coller)
dpkg -s xclip  &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\nxclip est installé. Continuons.\n"
else
    echo -e "\nxclip n'est pas installé. Il vous faut l'installer avant tout\n"
    echo -e "Saisissez cette commande et relancer le script ensuite.\n"
    echo -e "sudo apt-get install -y xclip\n"
    exit
fi

# ==============================
# Recupération des informations
# ==============================

# Identification du CPU
CPUNAME=$(lscpu | egrep "Nom de modèle :" | cut -f2 -d : | sed -e 's/^[ \t]*//')

# RAM
RAM=$(free -h | grep "Mem" | awk '{print $2}')

# Distribution
DISTRIB=$(hostnamectl | grep "Operating System" | cut -f2 -d :)

# Carte graphique
GRAPHICCARD=$(lspci | grep VGA | cut -f3 -d :)

# kernek version
KERNEL=$(uname -r)


# ==============================
# Mise en forme et push vers le presse papier
# ==============================

# Formatage HTML pour le forum
# Ligne 1 vers fichier temporaire
echo "[color=#FF0000]Distrib :[/color] [color=#00BF00]$DISTRIB[/color] [color=#FF0000]Desktop :[/color] [color=#00BF00]$XDG_CURRENT_DESKTOP [/color]" >> ~.tmp-sign

# Ligne 2 vers fichier temporaire
echo "[color=#FF0000]Kernel : [/color] $KERNEL [color=#FF0000]CPU : [/color] $CPUNAME [color=#FF0000]RAM : [/color] $RAM"  >> ~.tmp-sign

# Ligne 3 vers fichier temporaire
echo "[color=#FF0000]CG : [/color] $GRAPHICCARD"  >> ~.tmp-sign

# affichage sur sortie standard
# cat ~.tmp-sign

# Copie dans le presse papier 
cat ~.tmp-sign | xclip -selection clipboard



# ==============================
# Affichage message
# ==============================

echo "# ================================================================="
echo "Le fichier a été correctement copier dans le presse papier."
echo "Rendez vous sur la page profil de votre compte, section signature."
echo "Copier le (ctr + v)."
echo "# ================================================================="
echo ''
echo ''
echo ''
echo "En voici son contenu :"
echo ''
cat ~.tmp-sign

# Suppression fichier temporaire
rm  ~.tmp-sign

